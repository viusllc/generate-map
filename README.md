# Generate Map Plugin
Creates an easy to use wrapper for the Google Maps API.

## Requires
* jQuery
* Google Maps
* Google Maps api key

## Instructions
Create a jQuery selector and run .generateMap() generate map accepts several arguments. 
Ex: $('.test-map').generateMap();

See the tests for more detailed examples.

For the tests to work you will have to add your api key to the google URL, just like you will have to do when you're really developing with this or using the goole maps api barebones.
To get the ajax test to work visit the ajax test directory and type node index.js and visit  http://127.0.0.1:8125/ in your favorite web browser. - you need to do this because ajax can't pull from your local file system.


## Accepted Parameters
* markersFromDom -- string -- if you are loading in markers via the dom this is the selector you want to target, please include data-lat and data-lon as well as any other information you wish to access using the infoWindowText method.
* infoWindowText -- function -- returns the markup up you want the info window to have, it passes in a location object. If you are using markers from dom it will use that dom objects data attributes, if you are using ajax you can access the responses properties.
* mapOptions -- object -- the map options you would normally pass into the google map.
* infoWindowOptions -- object -- the info window objects you would normally pass into the google map.
* preMarkerZoom -- int -- the zoom level you want the map to be at before markers get added. It automatically resizes based on marker position.
* ajaxUrl -- string -- if you are making an ajax call set the URL you want to interact with here.
* ajaxParams -- json -- the information you want to pass to the ajaxURL.
* beforeInit -- function -- a function to call before initializing the map.
* afterInit -- function -- a function to call after intializing the map.
* afterAjaxCallback -- function -- a function to call at the end of each ajax request.
