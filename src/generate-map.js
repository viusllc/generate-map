/*global console:true, Modernizr:true, google:true, theme:true */
(function($){

	// jQuery interface for the user.
	$.fn.generateMap = function( params ){
		// these are the default settings
		var defaults = {
			selector : this,
			infoWindowOptions : { maxWidth : 600},
			mapOptions : {
				center: {lng: -95.712891, lat: 37.09024},
				disableDefaultUI : false,
				scrollwheel : false,
				zoomControl : true,
				draggable : true,
				styles : [
					{
						"featureType":"all",
						"elementType":"all",
						"stylers":[
							{"hue":"#ff8800"}
						]
					},
					{
						"featureType":"water",
						"elementType":"all",
						"stylers":[
							{"color":"#00badf"}
						]
					}
				]
			},
			preMarkerZoom : 4,

			// a function that gets the location objects
			// use these to set up the markup you need for the markers.
			infoWindowText : function( loc ){ 
				return '<div>Hello</div>';
			}
		};

		// tell the method to override the defaults with the user set parameters.
		var settings = $.extend({}, defaults, params);

		// call the true functions with the new settings.
		$.mapFacade.init( settings );
	};

	$.mapFacade = {
		first : true,

		/**
		 * Initialize the map
		 * @param  a javascript object of parameters
		 * params--
		 *	params.selector - the selector of the dom element you wish the map to become.
		 *	params.infoWindowOptions -- the infowindow options for instantiating infowindow
		 *  params.mapOptions -- base map options
		 *  params.preMarkerZoom -- what zoom level should the map be before markers are set (good to have incase there are no markers.)
		 *  params.infoWindowText -- the markup you want to display for each marker goes here. This should be injected as a function, the object you will pull data from is the argument named 'loc'. 
		 *  params.markersFromDom -- a selector, most likely a class as there will probably be multiple locations. use this if you already have all the data loaded into the dom. it will run an each function and pass the dom item into the infoWindowText argument.
		 *    if using this please add a data-lat and data-lon for lat and lon.
		 *  params.ajaxURL -- the url you want the ajax call to go to.
		 *  params.ajaxParams -- the parameters you want to send over via ajax, this is json.
		 */
		init : function(params) {

			$(params.selector).each(function(){

				// run the beforeInit if we have one.
				if (typeof params.beforeInit !== 'undefined' ){
					params.beforeInit();
				}

				var container = $(this);

				( function( container ) {

					var	map = new google.maps.Map( container.get(0), params.mapOptions ),
						infowindow = new google.maps.InfoWindow( params.infoWindowOptions ),
						bounds = new google.maps.LatLngBounds();

					//set zoom level on the map
					if ( typeof params.preMarkerZoom  !== 'undefined'){
						map.setZoom( params.preMarkerZoom );
					}
					//

					//
					//  load in data directly from the dom.
					//
					if ( typeof params.markersFromDom !== 'undefined'){
						$( params.markersFromDom ).each(function(){
							var item = $(this);

							var marker = new google.maps.Marker({
								position : new google.maps.LatLng( item.data('lat'), item.data('lon') ),
								map : map
							});

							// add the marker and set the text
							( function(marker, item){
								google.maps.event.addListener(marker, 'click', function() {
									infowindow.setContent( params.infoWindowText(item) );
									infowindow.open(map, marker);
								});
							})( marker, item );
							//

							bounds.extend(marker.position);
						});

						map.fitBounds(bounds);
						var zoomChangeBoundsListener = 
							google.maps.event.addListenerOnce(map, 'bounds_changed', function(event) {
								if (this.getZoom()){
									this.setZoom(12);
								}
						});
						setTimeout(function(){google.maps.event.removeListener(zoomChangeBoundsListener)}, 2000);

					}
					//
					// End load in data directly from dom.
					//

					// if we have ajax params make a call.
					if (typeof params.ajaxParams !== 'undefined'){
						// make an ajax call
						$.mapFacade.mapLocAjax(map, bounds, params);
					}

					// run the after init method if we have one 
					if (typeof params.afterInit !== 'undefined'){
						params.afterInit();
					}

				} )( container );
			});

		},

		// make an ajax call to get the lat and lon for the districts associated locations.
		mapLocAjax : function(map, bounds, params){
			$.post(
				params.ajaxURL,
				params.ajaxParams,
				function(res){
					// this loop will load in regular page data and map data...
					for (var i = 0; i < res.length; i++) {
						// map logic
						var contentStr = '<div>Loading...</div>';

						var infowindow = new google.maps.InfoWindow({
							content : contentStr
						});

						var marker = new google.maps.Marker({
							position : new google.maps.LatLng( res[i].lat, res[i].lon ),
							map : map
						});
						
						// add the marker, set the markers text.
						( function(marker, res){
							google.maps.event.addListener(marker, 'click', function() {							
								infowindow.setContent(  params.infoWindowText(res) );
								infowindow.open(map, marker);
							});
						})( marker, res[i] );
						//

						// rewrire the map bounds to respect the markers position.
						bounds.extend(marker.position);
						
						// end map logic
						// 

					}
					//
					
					// reset the bounds
					map.fitBounds(bounds);
					map.setCenter(bounds.getCenter());
					//
					
					// if there is an after ajaxcallback method, run it
					if (typeof params.afterAjaxCallback !== 'undefined'){
						params.afterAjaxCallback();
					}


					$.mapFacade.first = false;
				}
			);
		}

	};
})(jQuery);